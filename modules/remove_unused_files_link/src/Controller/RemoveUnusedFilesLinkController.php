<?php

namespace Drupal\remove_unused_files_link\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\remove_unused_files\RemoveUnusedFilesService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Remove unused files link controller.
 */
class RemoveUnusedFilesLinkController extends ControllerBase {

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The RemoveUnusedFiles service.
   *
   * @var \Drupal\remove_unused_files\RemoveUnusedFilesService
   */
  protected $removeUnusedFiles;

  /**
   * RemoveUnusedFilesLinkController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\remove_unused_files\RemoveUnusedFilesService $remove_unused_files
   *   The RemoveUnusedFiles service.
   */
  public function __construct(RequestStack $request_stack, RemoveUnusedFilesService $remove_unused_files) {
    $this->request = $request_stack->getCurrentRequest();
    $this->removeUnusedFiles = $remove_unused_files;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('remove_unused_files')
    );
  }

  /**
   * Adding flags to remove unused files for in next cron run.
   */
  public function exec() {
    $result = $this->removeUnusedFiles->exec();
    $this->messenger()->addMessage($this->t($result->messageString), $result->messageType); // phpcs:ignore
    return new RedirectResponse($this->request->headers->get('referer') ?? '/');
  }

}

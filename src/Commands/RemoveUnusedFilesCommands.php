<?php

namespace Drupal\remove_unused_files\Commands;

use Drupal\remove_unused_files\RemoveUnusedFilesService;
use Drush\Commands\DrushCommands;

/**
 * Remove unused files commands.
 */
class RemoveUnusedFilesCommands extends DrushCommands {

  /**
   * The remove unused files service.
   *
   * @var \Drupal\remove_unused_files\RemoveUnusedFilesService
   */
  private $removeUnusedFiles;

  /**
   * RemoveUnusedFilesCommands constructor.
   *
   * @param \Drupal\remove_unused_files\RemoveUnusedFilesService $remove_unused_files
   *   The RemoveUnusedFiles service.
   */
  public function __construct(RemoveUnusedFilesService $remove_unused_files) {
    $this->removeUnusedFiles = $remove_unused_files;
  }

  /**
   * Adding flags to remove unused files for in next cron run.
   *
   * @command remove_unused_files
   * @usage remove_unused_files
   */
  public function removeUnusedFiles() {
    $result = $this->removeUnusedFiles->exec();
    $this->logger()->log($result->logLevelDrush, \dt($result->messageString));
  }

}

<?php

namespace Drupal\remove_unused_files;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drush\Log\LogLevel as LogLevelDrush;
use Psr\Log\LogLevel as LogLevelPsr;

/**
 * Remove unused files service.
 */
class RemoveUnusedFilesService {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The message type.
   *
   * @var string
   */
  public $messageType;

  /**
   * The log level of Psr.
   *
   * @var string
   */
  public $logLevelPsr;

  /**
   * The log level of drush.
   *
   * @var string
   */
  public $logLevelDrush;

  /**
   * The message string.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|string
   */
  public $messageString;

  /**
   * RemoveUnusedFilesService constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Adding flags to remove unused files for in next cron run.
   *
   * @return \Drupal\remove_unused_files\RemoveUnusedFilesService
   *   Return current object.
   */
  public function exec() {
    try {
      $query = $this->connection->query(<<<'SQL'
      UPDATE
        {file_managed} AS b1
        , (
          SELECT
            DISTINCT {file_managed}.fid AS file_managed_fid
          FROM
            {file_managed}
          LEFT JOIN
            {file_usage} file_usage_file_managed
            ON {file_managed}.fid = {file_usage_file_managed}.fid
          GROUP BY
            {file_managed}.fid
          HAVING
            (COUNT(file_usage_file_managed.count) = 0)
        ) AS b2
      SET
        b1.status = 0
        , b1.created = 0
        , b1.changed = 0
      WHERE
        b1.fid = b2.file_managed_fid;
      SQL);
      $query->execute();
      $this->messageType = MessengerInterface::TYPE_STATUS;
      $this->logLevelPsr = LogLevelPsr::INFO;
      $this->logLevelDrush = LogLevelDrush::SUCCESS;
      $this->messageString = 'Succeed of unused managed files to temporary files change. Those files removal next time cron run.';
    }
    catch (\Exception $e) {
      $this->messageType = MessengerInterface::TYPE_ERROR;
      $this->logLevelPsr = LogLevelPsr::ERROR;
      $this->logLevelDrush = LogLevelDrush::ERROR;
      $this->messageString = $e->getMessage();
    }
    return $this;
  }

}
